const express = require('express');
const fetch = require('node-fetch');
const shopifyBuy = require('shopify-buy');
const bodyParser = require('body-parser');
const _ = require('lodash');
const cors = require('cors');
require('dotenv').config();

const app = express();

const server = app.listen(3000, () => {
    console.log('Server listening on port 3000!');
});

var io = require('socket.io')(server);

global.fetch = fetch;

app.use(bodyParser.json());
app.use(cors());

const client = shopifyBuy.buildClient({
    storefrontAccessToken: process.env.STOREFRONT_ACCESS_TOKEN,
    domain: process.env.DOMAIN
});

const shopInfoPromise = client.shop.fetchInfo();
const productPromise = client.product.fetchAll();

app.get('/', (req, res) => {
    let checkoutId = req.query.checkoutId;
    if (!checkoutId) {
        return client.checkout.create({}).then((checkout) => {
            checkoutId = checkout.id;
            res.redirect(`/?checkoutId=${checkoutId}`);
        })
    }

    let checkoutPromise = client.checkout.fetch(checkoutId);

    return Promise.all([shopInfoPromise, productPromise, checkoutPromise])
        .then(([shopInfo, products, checkout]) => {
            let viewModel = {
                productList: products,
                shop: shopInfo,
                checkout: checkout
            };

            res.status(200).send(viewModel);
        })
        .catch((err) => {
                res.status(400).send();
        });
});

app.post('/add-line-items/:productId', (req, res) => {
    let productId = req.params.productId;
    let options = req.body;
    let checkoutId = options.checkoutId;
    let quantity = options.quantity;

    delete options.checkoutId;
    delete options.quantity;

    return client.product.fetch(productId).then((product) => {
        let variant = client.product.helpers.variantForOptions(product, options);
        let variantId = variant.id;
        client.checkout.addLineItems(checkoutId, [{variantId: variantId, quantity: quantity}] );

        client.checkout.fetch(checkoutId).then((checkout) => {
            io.emit('UPDATE_CHECKOUT', checkout);
            io.emit('SHOW_CART_PANEL');
        })

        res.redirect(`/?checkoutId=${checkoutId}`);
    })
})
